package com.wschurman.colorapp;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class DataEntriesDataSource {
	
	private SQLiteDatabase db;
	private DataReaderDbHelper dbHelper;
	
	private String[] allColumns = {
		DataReaderContract.DataEntry._ID,
		DataReaderContract.DataEntry.COLUMN_NAME_TIME,
		DataReaderContract.DataEntry.COLUMN_NAME_LAT,
		DataReaderContract.DataEntry.COLUMN_NAME_LNG,
		DataReaderContract.DataEntry.COLUMN_NAME_AVG_COLOR,
		DataReaderContract.DataEntry.COLUMN_NAME_PHOTO_URI,
		DataReaderContract.DataEntry.COLUMN_NAME_STRESS_VAL
	};

	public DataEntriesDataSource(Context context) {
		dbHelper = new DataReaderDbHelper(context);
	}
	
	public void open() throws SQLException {
		db = dbHelper.getWritableDatabase();
	}
	
	public void close() {
		db.close();
	}
	
	public void deleteAllEntries() {
		dbHelper.resetTable(db);
	}
	
	public DataEntry createDataEntry(
			double lat, double lng, int avg_color, String photo_uri, double stress) {
		
		ContentValues values = new ContentValues();
		values.put(
			DataReaderContract.DataEntry.COLUMN_NAME_TIME,
			(new Timestamp((new Date()).getTime())).toString()
		);
		values.put(DataReaderContract.DataEntry.COLUMN_NAME_LAT, lat);
		values.put(DataReaderContract.DataEntry.COLUMN_NAME_LNG, lng);
		values.put(DataReaderContract.DataEntry.COLUMN_NAME_AVG_COLOR, avg_color);
		values.put(DataReaderContract.DataEntry.COLUMN_NAME_PHOTO_URI, photo_uri);
		values.put(DataReaderContract.DataEntry.COLUMN_NAME_STRESS_VAL, stress);
		
		long newRowId = db.insert(
			DataReaderContract.DataEntry.TABLE_NAME,
			null,
			values
		);
		
		Cursor cursor = db.query(
			DataReaderContract.DataEntry.TABLE_NAME,
			allColumns, 
			DataReaderContract.DataEntry._ID + " = " + newRowId,
			null, null, null, null
		);
		
		cursor.moveToFirst();
		DataEntry entry = fromCursor(cursor);
		cursor.close();
		
		return entry;
	}
	
	public List<DataEntry> getAllDataEntries(Date since) {
		List<DataEntry> entries = new ArrayList<DataEntry>();
		
		String selection = null, since_string = null, now_string = null;
		
		if (since != null) {
			now_string = (new Timestamp((new Date()).getTime())).toString();
			since_string = (new Timestamp(since.getTime())).toString();
			
			selection = " BETWEEN ? AND ? ";
		}
		
		Cursor cursor = db.query(
			DataReaderContract.DataEntry.TABLE_NAME,
			allColumns,
			selection,
			selection != null ? (new String[]{since_string, now_string}) : null,
			null, null, null
		);
		
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			entries.add(fromCursor(cursor));
			cursor.moveToNext();
		}
		cursor.close();
		
		return entries;
	}
	
	public List<DataEntry> getAllDataEntries() {
		return getAllDataEntries(null);
	}
	
	private DataEntry fromCursor(Cursor cursor) {
		DataEntry data = new DataEntry();
		data.setId(cursor.getLong(cursor.getColumnIndex(DataReaderContract.DataEntry._ID)));
		
		// datetime
		Timestamp ts = Timestamp.valueOf(
			cursor.getString(cursor.getColumnIndex(DataReaderContract.DataEntry.COLUMN_NAME_TIME))
		);
		data.setDatetime((Date)ts);
		
		// average color
		data.setAvgColor(cursor.getInt(cursor.getColumnIndex(DataReaderContract.DataEntry.COLUMN_NAME_AVG_COLOR)));
		
		// all other fields
		data.setLat(cursor.getDouble(cursor.getColumnIndex(DataReaderContract.DataEntry.COLUMN_NAME_LAT)));
		data.setLng(cursor.getDouble(cursor.getColumnIndex(DataReaderContract.DataEntry.COLUMN_NAME_LNG)));
		data.setImgURI(cursor.getString(cursor.getColumnIndex(DataReaderContract.DataEntry.COLUMN_NAME_PHOTO_URI)));
		data.setStress(cursor.getDouble(cursor.getColumnIndex(DataReaderContract.DataEntry.COLUMN_NAME_STRESS_VAL)));
		
		return data;
	}
}
