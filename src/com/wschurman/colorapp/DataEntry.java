package com.wschurman.colorapp;

import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

public class DataEntry {

	private long id;
	private Date datetime;
	private double lat;
	private double lng;
	
	// int from Color
	private int avg_color;
	
	private String imgURI;
	private double stress;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Date getDatetime() {
		return datetime;
	}
	public void setDatetime(Date datetime) {
		this.datetime = datetime;
	}
	public double getLat() {
		return lat;
	}
	public void setLat(double lat) {
		this.lat = lat;
	}
	public double getLng() {
		return lng;
	}
	public void setLng(double lng) {
		this.lng = lng;
	}
	public int getAvgColor() {
		return avg_color;
	}
	public void setAvgColor(int avgColor) {
		this.avg_color = avgColor;
	}
	public String getImgURI() {
		return imgURI;
	}
	public void setImgURI(String imgURI) {
		this.imgURI = imgURI;
	}
	public double getStress() {
		return stress;
	}
	public void setStress(double stress) {
		this.stress = stress;
	}
	
	@Override
	public String toString() {
		return id + ": " + lat + ", " + lng + "; " + avg_color + "; " + stress;
	}
	
	public JSONObject toJSON() throws JSONException {
		JSONObject json = new JSONObject();
		
		json.put("id", id);
		json.put("datetime", datetime);
		json.put("lat", lat);
		json.put("lng", lng);
		json.put("avg_color", avg_color);
		json.put("imgURI", imgURI);
		json.put("stress", stress);
		
		return json;
	}
	
	public static DataEntry fromJSON(JSONObject json) throws JSONException {
		DataEntry de = new DataEntry();
		
		de.setId(json.getLong("id"));
		de.setDatetime((Date)json.get("datetime"));
		de.setLat(json.getDouble("lat"));
		de.setLng(json.getDouble("lng"));
		de.setAvgColor(json.getInt("avg_color"));
		de.setImgURI(json.getString("imgURI"));
		de.setStress(json.getDouble("stress"));
		
		return de;
	}
}
