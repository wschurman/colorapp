package com.wschurman.colorapp;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;

public class ColorSequenceDrawable extends LayerDrawable {

	private ShapeDrawable[] layers;
	
	public ColorSequenceDrawable(Drawable[] layers) {
		super(layers);
		this.layers = (ShapeDrawable[]) layers;
	}
	
	@Override
	public void draw(Canvas canvas) {
		int x = 40;
		int y = 0;
		
		int width = (canvas.getWidth() - x) / layers.length;
		int height = canvas.getHeight();
		
		for (ShapeDrawable s : layers) {
			s.setBounds(x, y, x + width, y + height);
			s.draw(canvas);
			
			x += width;
		}
	}

}
