package com.wschurman.colorapp;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

public class ReminderReceiver extends BroadcastReceiver {

	private static final String TAG = "ReminderReceiver";
	
	private Context ctx;
	private int NOTIFICATION = R.string.reminder_service_started;
	
	@Override
	public void onReceive(Context context, Intent intent) {
		Log.d(TAG, "Recieved Alarm");
		ctx = context;
		showNotification();
	}
	
	private void showNotification() {

        // Set the icon, scrolling text and timestamp
		NotificationCompat.Builder mBuilder =
				new NotificationCompat.Builder(ctx)
				.setSmallIcon(R.drawable.ic_launcher)
				.setContentTitle(ctx.getString(R.string.notification_title))
				.setContentText(ctx.getString(R.string.notification_body))
				.setVibrate(new long[]{100, 200, 100, 500})
				.setAutoCancel(true);

		Intent resultIntent = new Intent(ctx, DisplayCaptureActivity.class);
		
		TaskStackBuilder stackBuilder = TaskStackBuilder.create(ctx);
		stackBuilder.addParentStack(MainActivity.class);
		stackBuilder.addNextIntent(resultIntent);
		
        PendingIntent resultPendingIntent =
        		stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        mBuilder.setContentIntent(resultPendingIntent);
        
        NotificationManager mNotificationManager =
        		(NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
        
        // Send the notification.
        mNotificationManager.notify(NOTIFICATION, mBuilder.build());
	}

}
