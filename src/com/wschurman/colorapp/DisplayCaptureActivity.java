package com.wschurman.colorapp;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Random;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

public class DisplayCaptureActivity extends FragmentActivity {
	
	private static final String TAG = "DisplayCaptureActivity";
	
	private static final String instructions_prefs_key = "instructions_shown";
	
	private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
	public static final String PIC_DIRECTORY = "color_pics" + File.separator;
	
	private String lastPhotoPath;
	
	private GPSTracker gps;
	
	private ArrayList<StressQuestion> stressQuestions = new ArrayList<StressQuestion>();
	private HashMap<Integer, StressQuestion> usedQuestions = new HashMap<Integer, StressQuestion>();
	private HashSet<Integer> seekBarIds = new HashSet<Integer>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_display_capture);
		
		showDialog();
		setupStressQuestions();
	}
	
	private void showDialog() {
		// check if displayed before
		SharedPreferences prefs = getPreferences(MODE_PRIVATE);		
		if (prefs.getBoolean(instructions_prefs_key, false)) {
			return;
		}
		
		// if not, display dialog
		DialogFragment newFragment = new InstructionDialogFragment();
	    newFragment.show(getSupportFragmentManager(), "instructions");
	    
	    SharedPreferences.Editor editor = prefs.edit();
	    editor.putBoolean(instructions_prefs_key, true);
	    editor.commit();
	}
	
	private void setupStressQuestions() {
		//Add Perceived Stress Scale questions
		addQ("You have felt that you were unable to control the important things in your life.", true);
		addQ("You have felt confident about your ability to handle your personal problems.", false);
		addQ("You have felt that things were going your way.", false);
		addQ("You have felt difficulties were piling up so high that you could not overcome them.", true);
		
		Random randomGenerator = new Random();
		
		addRandomQuestion(randomGenerator.nextInt(stressQuestions.size()), R.id.form_stress_label_1, R.id.form_stress_value_1, R.id.form_stress_seek_1);
		addRandomQuestion(randomGenerator.nextInt(stressQuestions.size()), R.id.form_stress_label_2, R.id.form_stress_value_2, R.id.form_stress_seek_2);
		addRandomQuestion(randomGenerator.nextInt(stressQuestions.size()), R.id.form_stress_label_3, R.id.form_stress_value_3, R.id.form_stress_seek_3);
		addRandomQuestion(randomGenerator.nextInt(stressQuestions.size()), R.id.form_stress_label_4, R.id.form_stress_value_4, R.id.form_stress_seek_4);
		
	}
	
	private void addRandomQuestion(int randomInt, int labelId, int valueId, int seekId){
		((TextView) findViewById(labelId)).setText(stressQuestions.get(randomInt).getText());
		usedQuestions.put(seekId, stressQuestions.get(randomInt));
		stressQuestions.remove(randomInt);
		linkSeekWithValue(((SeekBar) findViewById(seekId)), ((TextView) findViewById(valueId)));
		seekBarIds.add(seekId);
	}
	
	private void linkSeekWithValue(SeekBar seekBar, final TextView textView){
		seekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {       

		    @Override       
		    public void onStopTrackingTouch(SeekBar seekBar) {   }       
	
		    @Override       
		    public void onStartTrackingTouch(SeekBar seekBar) {  }       
	
		    @Override       
		    public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser) { 
		    	int displayValue = seekBar.getProgress()  - 2;
		    	if(displayValue > 0){
		    		textView.setText("+" + displayValue + "");
		    	} else{
		    		textView.setText(displayValue + "");
		    	}
		    	    
		    }       
	    });
	}

	private void addQ(String string, boolean b) {
		stressQuestions.add(new StressQuestion(string, b));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.display_capture, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void getLocation() {
        gps = new GPSTracker(this);

        if(gps.canGetLocation()){
            gps.getLatitude();
            gps.getLongitude();
            Toast.makeText(getApplicationContext(), getString(R.string.location_toast), Toast.LENGTH_SHORT).show();
        } else {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gps.showSettingsAlert();
        }
	}
	
	public void doCapturePhoto(View view) throws IOException {
		// check that GPS is on and we have location
		getLocation();
		
		File f = createImageFile();
		Log.e(TAG, "PHOTOFILE: " + Uri.fromFile(f));
		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
		startActivityForResult(takePictureIntent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
	}
	
	private File createImageFile() throws IOException {
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
		String path = Environment.getExternalStorageDirectory() + File.separator + PIC_DIRECTORY;
		String imageFileName = path + timeStamp + ".jpg";
		if (!(new File(path)).exists()) {
			new File(path).mkdirs();
		}
		File image = new File(imageFileName);
		lastPhotoPath = image.getAbsolutePath();
		return image;
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
		if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
	        if (resultCode == RESULT_OK) {
	        	processImage(intent);
	        	finish();
	        } else if (resultCode == RESULT_CANCELED) {
	            // User cancelled the image capture
	        	Log.e(TAG, "Image Capture Cancelled");
	        } else {
	            // Image capture failed, advise user
	        	Log.e(TAG, "Image Capture Failed");
	        }
		}
    }
	
	private void processImage(Intent intent) {
		
		// average color
		BitmapFactory.Options options = new BitmapFactory.Options();
		Bitmap bitmap = BitmapFactory.decodeFile(lastPhotoPath, options);
		int averageColor = calculateAverageColor(bitmap);
		bitmap.recycle();
		
		
		// should be already set
		double lat = gps.getLatitude();
		double lng = gps.getLongitude();
		
		String photo_uri = lastPhotoPath;
		
		double stress = calculateStress();
		
		DataEntriesDataSource db = new DataEntriesDataSource(this);
		db.open();
		DataEntry de = db.createDataEntry(
			lat, lng,
			averageColor,
			photo_uri,
			stress
		);
		db.close();
		
		Toast.makeText(
				getApplicationContext(),
				R.string.data_entry_recorded_successfully,
				Toast.LENGTH_LONG).show();
		
		Log.d(TAG, "inserted id = "+de.getId());
	}
	
	private int calculateAverageColor(Bitmap bitmap) {
		long redBucket = 0;
		long greenBucket = 0;
		long blueBucket = 0;
		long pixelCount = 0;
		
		for (int y = 0; y < bitmap.getHeight(); y++) {
			for (int x = 0; x < bitmap.getWidth(); x++) {
				int c = bitmap.getPixel(x, y);
				
				pixelCount++;
				redBucket += Color.red(c);
				greenBucket += Color.green(c);
				blueBucket += Color.blue(c);
			}
		}
		
		redBucket = redBucket / pixelCount;
		greenBucket = greenBucket / pixelCount;
		blueBucket = blueBucket / pixelCount;
		
		int addVal = 30;
		
		if (redBucket >= greenBucket && redBucket >= blueBucket) {
			redBucket = Math.min(redBucket + addVal, 255); 
		} else if (blueBucket >= greenBucket && blueBucket >= redBucket) {
			blueBucket = Math.min(blueBucket + addVal, 255); 
		} else if (greenBucket >= redBucket && greenBucket > blueBucket) {
			greenBucket = Math.min(greenBucket + addVal, 255); 
		}
		
		return Color.rgb(
			(int)redBucket,
			(int)greenBucket,
			(int)blueBucket
		);
	}
	
	private double calculateStress() {
		
		int total = 0;
		
		for(Integer seekBarId : seekBarIds){
			
			if(usedQuestions.get(seekBarId).getPositiveStress()){
				total += ((SeekBar) findViewById(seekBarId)).getProgress();
			} else {//Reverse score
				total += (((SeekBar) findViewById(seekBarId)).getProgress() - 4) * -1;
			}
		}
		
		// normalize
		return total / (double)(((SeekBar) findViewById(R.id.form_stress_seek_1)).getMax() * usedQuestions.size());
	}
	
	private class StressQuestion{
		String text;
		
		//If the question purports more stress, this is positive
		//If it means less stress, it is negative
		boolean positiveStress;
		
		private StressQuestion(String text, boolean positiveStress){
			this.text = text;
			this.positiveStress = positiveStress;
		}
		
		private String getText(){
			return text;
		}
		
		private boolean getPositiveStress(){
			return positiveStress;
		}	
	}
	
	public static class InstructionDialogFragment extends DialogFragment {
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			// Use the Builder class for convenient dialog construction
	        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
	        builder.setTitle(R.string.instructions_title)
	        	   .setMessage(R.string.instructions)
	               .setPositiveButton("OK", new DialogInterface.OnClickListener() {
	                   public void onClick(DialogInterface dialog, int id) {
	                	   dialog.dismiss();
	                   }
	               });
	        // Create the AlertDialog object and return it
	        return builder.create();
		}
	}
}
