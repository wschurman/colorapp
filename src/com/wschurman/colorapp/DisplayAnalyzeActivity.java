package com.wschurman.colorapp;

import java.util.List;

import android.app.Activity;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GraphView.GraphViewData;
import com.jjoe64.graphview.GraphViewSeries;
import com.jjoe64.graphview.LineGraphView;

public class DisplayAnalyzeActivity extends Activity {

	private static final String TAG = "DisplayAnalyzeActivity";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_display_analyze);
		
		setupContent();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.display_analyze, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void setupContent() {
		Log.d(TAG, "filling table with data entries");
		
		DataEntriesDataSource ds = new DataEntriesDataSource(this);
		ds.open();
		List<DataEntry> entries = ds.getAllDataEntries();
		ds.close();
		
		if (entries.size() < 2) {
			Toast.makeText(getApplicationContext(), R.string.analyze_warning, Toast.LENGTH_SHORT).show();
			finish();
			return;
		}
		
		LinearLayout container = (LinearLayout) findViewById(R.id.analyze_container);
		
		GraphViewData[] gdata = new GraphViewData[entries.size()];
		ShapeDrawable[] colors = new ShapeDrawable[entries.size()];
		
		for (int i = 0; i < entries.size(); i++) {
			gdata[i] = new GraphViewData(i, entries.get(i).getStress());
			
			ShapeDrawable d = new ShapeDrawable(new RectShape());
			d.getPaint().setColor(entries.get(i).getAvgColor());
			colors[i] = d;
		}
		GraphViewSeries series = new GraphViewSeries(gdata);
		GraphView graphView = new LineGraphView(this, "Stress");
		graphView.setBackgroundDrawable(new ColorSequenceDrawable(colors));
		graphView.addSeries(series);
		container.addView(graphView);
	}
}
