package com.wschurman.colorapp;

import java.io.File;
import java.util.Calendar;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends Activity {
	
	private static final String TAG = "MainActivity";
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
    public void doCapture(View view) {
    	Intent intent = new Intent(this, DisplayCaptureActivity.class);
    	startActivity(intent);
    }
    
    public void doAnalyze(View view) {
    	Intent intent = new Intent(this, DisplayAnalyzeActivity.class);
    	startActivity(intent);
    }
    
    public void doDeleteData(View view) {
    	DataEntriesDataSource ds = new DataEntriesDataSource(this);
    	ds.open();
    	ds.deleteAllEntries();
    	ds.close();
    	
    	// delete photos
    	String dir = DisplayCaptureActivity.PIC_DIRECTORY;
    	String path = Environment.getExternalStorageDirectory() + File.separator + dir;
    	File d = new File(path);
    	if (!d.exists()) {
			return;
		} else {
			if (deleteDirectory(d)) {
				Toast.makeText(this, R.string.data_deleted_successfully, Toast.LENGTH_LONG).show();
			} else {
				Toast.makeText(this, R.string.error_deleting_data, Toast.LENGTH_LONG).show();
			}
		}
    }
    
    private static boolean deleteDirectory(File path) {
    	boolean ret = true;
    	if (path.exists()) {
    		File[] files = path.listFiles();
    		if (files == null) {
    			return true;
    		}
    		for (int i = 0; i < files.length; i++) {
    			if (files[i].isDirectory()) {
    				ret &= deleteDirectory(files[i]);
    			} else {
    				ret &= files[i].delete();
    			}
    		}
    	}
    	return ret & path.delete();
    }
    
    public void startReminders(View view) {
    	AlarmManager mgr=(AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
    	Intent i = new Intent(this, ReminderReceiver.class);
    	PendingIntent pi = PendingIntent.getBroadcast(this, 12345, i, PendingIntent.FLAG_UPDATE_CURRENT);
    	
    	Calendar cal = Calendar.getInstance();
    	cal.add(Calendar.MINUTE, 30);
    	
    	mgr.cancel(pi);
    	mgr.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), AlarmManager.INTERVAL_HALF_HOUR, pi);
    	
    	Toast.makeText(this, R.string.started_reminder_notifications, Toast.LENGTH_SHORT).show();
    }
    
    public void stopReminders(View view) {
    	AlarmManager mgr=(AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
    	
    	Intent i = new Intent(this, ReminderReceiver.class);
    	PendingIntent pi = PendingIntent.getBroadcast(this, 12345, i, PendingIntent.FLAG_UPDATE_CURRENT);
    	
    	try {
    		mgr.cancel(pi);
    	} catch (Exception e) {
    		Log.e(TAG, "Could not cancel alarms", e);
    	}
    	
    	
    	Toast.makeText(this, R.string.stopped_reminder_notifications, Toast.LENGTH_SHORT).show();
    }
    
    public void doExportData(View view) throws JSONException {
    	DataEntriesDataSource ds = new DataEntriesDataSource(this);
    	ds.open();
    	List<DataEntry> entries = ds.getAllDataEntries();
    	ds.close();
    	
    	JSONArray json = new JSONArray();
    	
    	for (DataEntry entry : entries) {
    		json.put(entry.toJSON());
    	}
    	
    	Intent sendIntent = new Intent();
    	sendIntent.setAction(Intent.ACTION_SEND);
    	sendIntent.putExtra(Intent.EXTRA_TEXT, json.toString());
    	sendIntent.setType("text/plain");
    	startActivity(Intent.createChooser(sendIntent, getString(R.string.share_export_to)));
    }
}
