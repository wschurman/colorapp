package com.wschurman.colorapp;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataReaderDbHelper extends SQLiteOpenHelper {
	
	public static final int DATABASE_VERSION = 2;
	public static final String DATABASE_NAME = "DataReader.db";
	
	private static final String SQL_CREATE_TABLE = 
		"CREATE TABLE " + DataReaderContract.DataEntry.TABLE_NAME + " (" +
		DataReaderContract.DataEntry._ID + " INTEGER PRIMARY KEY," +
		DataReaderContract.DataEntry.COLUMN_NAME_TIME + " DATETIME," +
		DataReaderContract.DataEntry.COLUMN_NAME_LAT + " DECIMAL(9,6)," +
		DataReaderContract.DataEntry.COLUMN_NAME_LNG + " DECIMAL(9,6)," +
		DataReaderContract.DataEntry.COLUMN_NAME_PHOTO_URI + " TEXT," +
		DataReaderContract.DataEntry.COLUMN_NAME_STRESS_VAL + " DOUBLE," +
		DataReaderContract.DataEntry.COLUMN_NAME_AVG_COLOR + " INTEGER" +
		" )";
	
	private static final String SQL_DELETE_TABLE =
		"DROP TABLE IF EXISTS " + DataReaderContract.DataEntry.TABLE_NAME;
	
	public DataReaderDbHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(SQL_CREATE_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL(SQL_DELETE_TABLE);
		onCreate(db);
	}
	
	public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		onUpgrade(db, oldVersion, newVersion);
	}

	public void resetTable(SQLiteDatabase db) {
		db.execSQL(SQL_DELETE_TABLE);
		onCreate(db);
	}
}
