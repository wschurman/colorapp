package com.wschurman.colorapp;

import android.provider.BaseColumns;

public class DataReaderContract {
	
	private DataReaderContract() {}
	
	public abstract class DataEntry implements BaseColumns {
		public static final String TABLE_NAME = "entries";
		public static final String COLUMN_NAME_TIME = "dt";
		public static final String COLUMN_NAME_LAT = "lat";
		public static final String COLUMN_NAME_LNG = "lng";
		public static final String COLUMN_NAME_AVG_COLOR = "avgcolor";
		public static final String COLUMN_NAME_PHOTO_URI = "photouri";
		public static final String COLUMN_NAME_STRESS_VAL = "stress";
	}
}


